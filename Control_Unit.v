`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 22.04.2020 19:10:26
// Design Name: 
// Module Name: Control_Unit
// Project Name: Pipeline Processor
// Target Devices: 
// Tool Versions: 
// Description: Generates control signals given opcode
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Control_Unit(input[1:0] opcode,output reg write,output reg ALUop, output reg ALUSrc);
//Generates write, ALUSrc, ALUop on change in opcode
always@(opcode)
begin
    case(opcode)
        2'b00:begin//mov
            write <= 1;
            ALUop <= 0;
            ALUSrc <= 0;
        end
        2'b01:begin//add
            write <= 1;
            ALUop <= 1;
            ALUSrc <= 1;
        end
        default:begin//Incorrect opcodes and j
            write <= 0;
            ALUop <= 0;
            ALUSrc <= ALUSrc;
        end
    endcase
end
endmodule
