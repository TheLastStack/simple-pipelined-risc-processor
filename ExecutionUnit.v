`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 22.04.2020 19:28:27
// Design Name: 
// Module Name: ExecutionUnit
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Finds result. 
// 
// Dependencies: ALU
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ExecutionUnit(input ALUop, input ALUSrc, input[7:0] redreg1, input[7:0] redreg2, output reg[7:0] result);
//Stores result from ALU
wire[7:0] ALUresult;
//Performing operation
ALU Unit1(ALUop,redreg1,redreg2,ALUresult);
//Deciding overall result based on ALUSrc
always@(ALUresult or ALUSrc or ALUop or redreg1 or redreg2)
begin
    if(ALUSrc==1)
        result <= ALUresult;
    else
        result <= redreg2;
end
endmodule
