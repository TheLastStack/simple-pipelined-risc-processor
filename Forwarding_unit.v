`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 22.04.2020 19:38:56
// Design Name: 
// Module Name: Forwarding_unit
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Checks forwarding condition and implements backpropagation
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Forwarding_unit(input EXWBwrite, input[2:0] EXWBrd, input[7:0] EXWBresult, input[2:0] IDEXrs1, input[2:0] IDEXrs2, input[7:0] IDEXread1, input[7:0] IDEXread2, output reg[7:0] IDEX1, output reg[7:0] IDEX2);
//On any change in inputs, checks whether ID/EX rs1 or rs2 equals EX/WB rd and loads appropriate data.
always@(*)
begin
    if(EXWBwrite==1)//Previous Instruction writes to reg file
    begin
        if(IDEXrs1==EXWBrd)//Backpropagation for rs1
            IDEX1 <= EXWBresult;
        else
            IDEX1 <= IDEXread1;
        if(IDEXrs2==EXWBrd)//Backpropagation for rs2
            IDEX2 <= EXWBresult;
        else
            IDEX2 <= IDEXread2;
    end
    else//otherwise send read data
    begin
        IDEX1 <= IDEXread1;
        IDEX2 <= IDEXread2;
    end
end
endmodule
