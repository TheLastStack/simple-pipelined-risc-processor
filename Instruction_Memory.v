`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N.Mahen
// 
// Create Date: 20.04.2020 22:57:33
// Design Name: 
// Module Name: Instruction_Memory
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Asynchronously initialized instruction memory and returns instructions.
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Instruction_Memory(input reset, input[7:0] inPC, output[7:0] redmem);
//Instructions
reg[7:0] internal[127:0];
//Reading
assign redmem=internal[inPC];
//Asynchronous loading of instructions
always@(posedge reset)
begin
    $readmemh("pipemem.mem",internal);
end
endmodule
