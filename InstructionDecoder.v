`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 20.04.2020 23:45:22
// Design Name: 
// Module Name: InstructionDecoder
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Decodes instructions, sends opcode to control unit, and reading/writing to regfile.
// 
// Dependencies: Register_Memory
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionDecoder(input reset,input write,input[2:0] writereg,input[7:0] writedata,input[7:0] instr,output[2:0] rs1,output[2:0] rs2,output[2:0] rd,output[7:0] redreg1,output[7:0] redreg2,output [1:0] opcode);
assign rs1=instr[5:3];
assign rs2=instr[2:0];
assign rd=instr[5:3];
//Registers being used
//Opcode for control unit
assign opcode=instr[7:6];
//Register memory
Register_Memory RM1(reset,instr[5:3],instr[2:0],write,writereg,writedata,redreg1,redreg2);
endmodule
