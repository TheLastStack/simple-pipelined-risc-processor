`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 22.04.2020 19:19:07
// Design Name: 
// Module Name: ALU
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Performs selected operation on received operands.
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU(input ALUop, input[7:0] redreg1, input[7:0] redreg2, output reg[7:0] wro);
//ALUop can also serve as power gating for ALU. This is the reason it is set to 0 for j and mov
always@(ALUop or redreg1 or redreg2)
begin
    if (ALUop==1'b1)//add
        wro<=redreg1+redreg2;
    else//return 0
        wro<=0;
end
endmodule
