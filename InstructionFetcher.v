`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 20.04.2020 23:20:35
// Design Name: 
// Module Name: InstructionFetcher
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Fetches instructions synchronously. PC has a synchronous reset.
// 
// Dependencies: Instruction_Memory, PCUpdate
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionFetcher(input clk,input reset,output[7:0] read_instruction);
wire[7:0] PC;
//Connecting PC to required blocks
Instruction_Memory IM1(reset,PC,read_instruction);
PCUpdate PC1(clk,reset,PC,read_instruction,PC);
endmodule
