`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 20.04.2020 23:36:14
// Design Name: 
// Module Name: Register_Memory
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Simulates register file in a pipelined processor
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Register_Memory(input reset,input[2:0] readreg1,input[2:0] readreg2,input write,input[2:0] writereg,input[7:0] writedata,output[7:0] redreg1,output[7:0] redreg2);
//Register File
reg[7:0] internal[7:0];
//Reading
assign redreg1=internal[readreg1];
assign redreg2=internal[readreg2];
//Asynchronously initializing register file
always@(posedge reset)
    $readmemh("regit.mem",internal);
//Writing to memory
always@(write or writereg or writedata)
    if(write==1)
        internal[writereg]=writedata;
endmodule
