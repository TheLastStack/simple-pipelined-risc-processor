`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 20.04.2020 23:29:35
// Design Name: 
// Module Name: PipeReg
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Simulates synchronous reset registers.
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PipeReg#(parameter N=16)(input clk,input reset,input[N-1:0] in,output reg[N-1:0] out);
//Takes number of flip flops as input.
//Flip flop to synchronously detect reset posedge
reg preset;
always@(posedge clk)
begin
    out = in;//D flip flop
    case({preset,reset})//reset on posedge reset
        2'bx1:out=0;
        2'b01:out=0;
    endcase
    preset=reset;
end
endmodule
