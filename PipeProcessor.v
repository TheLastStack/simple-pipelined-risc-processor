`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2020 20:55:54
// Design Name: 
// Module Name: PipeProcessor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PipeProcessor(input clk, input reset);
//Declaring wires for connection between different pipeline stages
wire WBwrite,IDwrite,IDALUSrc,IDALUop,EXwrite,EXALUSrc,EXALUop;
wire[1:0] opcode;
wire[2:0] WB_rd,ID_rs1,ID_rs2,ID_rd,EX_rs1,EX_rs2,EX_rd;
wire[7:0] IF_instr,ID_instr,WB_result,ID_reg1,ID_reg2,EX_reg1,EX_reg2,EX_result,FU_reg1,FU_reg2;
//Connecting Instruction Fetch
InstructionFetcher IF1(clk,reset,IF_instr);
PipeReg #(8) IFID(clk, reset, IF_instr,ID_instr);
//Connecting Instruction Decoder and control unit
InstructionDecoder adec(reset,WBwrite,WB_rd,WB_result,ID_instr,ID_rs1,ID_rs2,ID_rd,ID_reg1,ID_reg2,opcode);
Control_Unit Controller(opcode,IDwrite,IDALUop,IDALUSrc);
PipeReg #(28) IDEX(clk,reset,{ID_reg1,ID_reg2,ID_rs1,ID_rs2,ID_rd,IDwrite,IDALUSrc,IDALUop},{EX_reg1,EX_reg2,EX_rs1,EX_rs2,EX_rd,EXwrite,EXALUSrc,EXALUop});
//Connecting forwarding logic and execution unit
Forwarding_unit Backprop(WBwrite,WB_rd,WB_result,EX_rs1,EX_rs2,EX_reg1,EX_reg2,FU_reg1,FU_reg2);
ExecutionUnit EU(EXALUop,EXALUSrc,FU_reg1,FU_reg2,EX_result);
PipeReg #(12) EXWB(clk,reset,{EX_result,EX_rd,EXwrite},{WB_result,WB_rd,WBwrite});
endmodule
