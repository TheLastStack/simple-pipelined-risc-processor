Simple Pipelined RISC Processor

A 4 stage pipelined processor with forwarding. Opcode is 2 bits, register locations have 3 bits.

|Operation|Format|
|---------|------|
|mov|`00 Rdest Rsrc`|
|add|`01 Rdest Rsrc`|
|j|`11 Instruction Field`|

