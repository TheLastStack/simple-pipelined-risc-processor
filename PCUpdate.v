`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 22.04.2020 18:41:49
// Design Name: 
// Module Name: PCUpdate
// Project Name: Pipelined Processor
// Target Devices: 
// Tool Versions: 
// Description: Performs PC update for jump, reset and normal cases
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PCUpdate(input clk, input reset, input[7:0] PCi, input[7:0] read_instr,output reg[7:0] PCo);
//Flip flop for synchronous loading of instructions
reg preset;
always@(posedge clk)
begin
    case({preset,reset})
        2'bx1:  PCo = 0;
        2'b01:  PCo = 0;//Posedge reset
        2'b11:  PCo = PCi+1;//Incrementing PC if reset is active
        default:PCo = PCi;//Do nothing when reset is 0
    endcase
    if (read_instr[7:6]==2'b11)
        PCo = {PCi[7:6],read_instr[5:0]};//Jump using pseudo-direct addressing
    preset=reset;
end
endmodule
